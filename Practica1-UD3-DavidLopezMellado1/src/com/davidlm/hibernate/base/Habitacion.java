/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package com.davidlm.hibernate.base;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;
/**
 * Clase Habitacion. Con sus correspondientes campos.
 */
@Entity
@Table(name = "habitaciones", schema = "basehotel", catalog = "")
public class Habitacion {
    private int id;
    private int numeroHabitacion;
    private String tipo;
    private double precioNoche;
    private String vistas;
    private String extras;
    private List<ClienteHabitacion> reservashabitacioncliente;
    private List<HabitacionAgencia> reservashabitacionagencia;

    /**
     * Indica que es el campo id de la tabla, también el nombre de la columna, el método para obtener el dato y el método para modificarlo.
     * @return
     */
    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    /**
     * Indica que es un atributo de la tabla(numero_habitacion), también el nombre de la columna, el método para obtener el dato y modificarlo.
     *
     * @return
     */
    @Basic
    @Column(name = "numero_habitacion")
    public int getNumeroHabitacion() {
        return numeroHabitacion;
    }

    public void setNumeroHabitacion(int numeroHabitacion) {
        this.numeroHabitacion = numeroHabitacion;
    }

    /**
     * Indica que es un atributo de la tabla(tipo), también el nombre de la columna, el método para obtener el dato y modificarlo.
     *
     * @return
     */
    @Basic
    @Column(name = "tipo")
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    /**
     * Indica que es un atributo de la tabla(precio_noche), también el nombre de la columna, el método para obtener el dato y modificarlo.
     *
     * @return
     */
    @Basic
    @Column(name = "precio_noche")
    public double getPrecioNoche() {
        return precioNoche;
    }

    public void setPrecioNoche(double precioNoche) {
        this.precioNoche = precioNoche;
    }

    /**
     * Indica que es un atributo de la tabla(vistas), también el nombre de la columna, el método para obtener el dato y modificarlo.
     *
     * @return
     */
    @Basic
    @Column(name = "vistas")
    public String getVistas() {
        return vistas;
    }

    public void setVistas(String vistas) {
        this.vistas = vistas;
    }

    /**
     * Indica que es un atributo de la tabla(extras), también el nombre de la columna, el método para obtener el dato y modificarlo.
     *
     * @return
     */
    @Basic
    @Column(name = "extras")
    public String getExtras() {
        return extras;
    }

    public void setExtras(String extras) {
        this.extras = extras;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Habitacion that = (Habitacion) o;
        return id == that.id &&
                numeroHabitacion == that.numeroHabitacion &&
                Double.compare(that.precioNoche, precioNoche) == 0 &&
                Objects.equals(tipo, that.tipo) &&
                Objects.equals(vistas, that.vistas) &&
                Objects.equals(extras, that.extras);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, numeroHabitacion, tipo, precioNoche, vistas, extras);
    }

    /**
     * Relación de uno a muchos. Una habitacion puede tener varias reservas de cliente.
     * @return
     */
    @OneToMany(mappedBy = "habitacion")
    public List<ClienteHabitacion> getReservashabitacioncliente() {
        return reservashabitacioncliente;
    }

    public void setReservashabitacioncliente(List<ClienteHabitacion> reservashabitacioncliente) {
        this.reservashabitacioncliente = reservashabitacioncliente;
    }

    /**
     * Relación de uno a muchos. Una habitacion puede tener varias reservas de agencia.
     * @return
     */
    @OneToMany(mappedBy = "habitacion")
    public List<HabitacionAgencia> getReservashabitacionagencia() {
        return reservashabitacionagencia;
    }

    public void setReservashabitacionagencia(List<HabitacionAgencia> reservashabitacionagencia) {
        this.reservashabitacionagencia = reservashabitacionagencia;
    }
}
