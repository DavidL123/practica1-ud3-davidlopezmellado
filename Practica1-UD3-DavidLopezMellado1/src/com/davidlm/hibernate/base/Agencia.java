/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package com.davidlm.hibernate.base;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;
/**
 * Clase Agencia que representa la entidad. Con sus correspondientes campos.
 */
@Entity
@Table(name = "agencias", schema = "basehotel", catalog = "")
public class Agencia {
    private int id;
    private String nombre;
    private String codigoEmpresa;
    private Date fechaCreacion;
    private int telefono;
    private String direccion;
    private String correo;
    private List<HabitacionAgencia> reservashabitacionagencia;
    /**
     * Indica que es el campo id de la tabla, también el nombre de la columna, el método para obtener el dato y el método para modificarlo.
     * @return
     */
    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    /**
     * Indica que es un atributo de la tabla (nombre), también el nombre de la columna, el método para obtener el dato y modificarlo.
     *
     * @return
     */

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Indica que es un atributo de la tabla(codigo_empresa), también el nombre de la columna, el método para obtener el dato y modificarlo.
     *
     * @return
     */
    @Basic
    @Column(name = "codigo_empresa")
    public String getCodigoEmpresa() {
        return codigoEmpresa;
    }

    public void setCodigoEmpresa(String codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }

    /**
     * Indica que es un atributo de la tabla(fecha_creacion), también el nombre de la columna y el método para obtener el dato.
     * @return
     */
    @Basic
    @Column(name = "fecha_creacion")
    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    /**
     * Indica que es un atributo de la tabla(telefono), también el nombre de la columna y el método para obtener el dato.
     * @return
     */
    @Basic
    @Column(name = "telefono")
    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    /**
     * Indica que es un atributo de la tabla(direccion), también el nombre de la columna y el método para obtener el dato.
     * @return
     */
    @Basic
    @Column(name = "direccion")
    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }


    /**
     * Indica que es un atributo de la tabla(correo), también el nombre de la columna y el método para obtener el dato.
     * @return
     */
    @Basic
    @Column(name = "correo")
    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Agencia agencia = (Agencia) o;
        return id == agencia.id &&
                telefono == agencia.telefono &&
                Objects.equals(nombre, agencia.nombre) &&
                Objects.equals(codigoEmpresa, agencia.codigoEmpresa) &&
                Objects.equals(fechaCreacion, agencia.fechaCreacion) &&
                Objects.equals(direccion, agencia.direccion) &&
                Objects.equals(correo, agencia.correo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, codigoEmpresa, fechaCreacion, telefono, direccion, correo);
    }

    /**
     * Relación de uno a muchos. Una agencia puede tener muchas reservas.
     * Getter y setter.
     * @return
     */
    @OneToMany(mappedBy = "agencia")
    public List<HabitacionAgencia> getReservashabitacionagencia() {
        return reservashabitacionagencia;
    }

    public void setReservashabitacionagencia(List<HabitacionAgencia> reservashabitacionagencia) {
        this.reservashabitacionagencia = reservashabitacionagencia;
    }

}
