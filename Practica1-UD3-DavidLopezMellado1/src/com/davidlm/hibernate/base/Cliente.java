/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package com.davidlm.hibernate.base;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

/**
 * Clase Cliente. Con sus correspondientes campos.
 */
@Entity
@Table(name = "clientes", schema = "basehotel", catalog = "")
public class Cliente {
    private int id;
    private String nombre;
    private String apellidos;
    private Date fechaNacimiento;
    private String domicilio;
    private String dni;
    private int numeroMovil;
    private String tipoCliente;
    private List<Pedido> pedidos;
    private List<ClienteHabitacion> reservasclientehabitacion;

    /**
     * Indica que es el campo id de la tabla, también el nombre de la columna, el método para obtener el dato y el método para modificarlo.
     * @return
     */
    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    /**
     * Indica que es un atributo de la tabla(nombre), también el nombre de la columna, el método para obtener el dato y modificarlo.
     *
     * @return
     */
    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Indica que es un atributo de la tabla(apellidos), también el nombre de la columna, el método para obtener el dato y modificarlo.
     *
     * @return
     */
    @Basic
    @Column(name = "apellidos")
    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    /**
     * Indica que es un atributo de la tabla(fecha_nacimiento), también el nombre de la columna, el método para obtener el dato y modificarlo.
     *
     * @return
     */
    @Basic
    @Column(name = "fecha_nacimiento")
    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    /**
     * Indica que es un atributo de la tabla(domicilio), también el nombre de la columna, el método para obtener el dato y modificarlo.
     *
     * @return
     */
    @Basic
    @Column(name = "domicilio")
    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    /**
     * Indica que es un atributo de la tabla(dni), también el nombre de la columna, el método para obtener el dato y modificarlo.
     *
     * @return
     */
    @Basic
    @Column(name = "dni")
    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    /**
     * Indica que es un atributo de la tabla(numero_movil), también el nombre de la columna, el método para obtener el dato y modificarlo.
     *
     * @return
     */
    @Basic
    @Column(name = "numero_movil")
    public int getNumeroMovil() {
        return numeroMovil;
    }

    public void setNumeroMovil(int numeroMovil) {
        this.numeroMovil = numeroMovil;
    }

    /**
     * Indica que es un atributo de la tabla(tipo_cliente), también el nombre de la columna, el método para obtener el dato y modificarlo.
     *
     * @return
     */
    @Basic
    @Column(name = "tipo_cliente")
    public String getTipoCliente() {
        return tipoCliente;
    }

    public void setTipoCliente(String tipoCliente) {
        this.tipoCliente = tipoCliente;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cliente cliente = (Cliente) o;
        return id == cliente.id &&
                numeroMovil == cliente.numeroMovil &&
                Objects.equals(nombre, cliente.nombre) &&
                Objects.equals(apellidos, cliente.apellidos) &&
                Objects.equals(fechaNacimiento, cliente.fechaNacimiento) &&
                Objects.equals(domicilio, cliente.domicilio) &&
                Objects.equals(dni, cliente.dni) &&
                Objects.equals(tipoCliente, cliente.tipoCliente);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, apellidos, fechaNacimiento, domicilio, dni, numeroMovil, tipoCliente);
    }

    /**
     * Relación de uno a muchos. Un cliente puede tener varios pedidos.
     * @return
     */
    @OneToMany(mappedBy = "cliente")
    public List<Pedido> getPedidos() {
        return pedidos;
    }

    public void setPedidos(List<Pedido> pedidos) {
        this.pedidos = pedidos;
    }

    /**
     * Relación de uno a muchos. Un cliente puede tener varias reservas.
     * @return
     */
    @OneToMany(mappedBy = "cliente")
    public List<ClienteHabitacion> getReservasclientehabitacion() {
        return reservasclientehabitacion;
    }

    public void setReservasclientehabitacion(List<ClienteHabitacion> reservasclientehabitacion) {
        this.reservasclientehabitacion = reservasclientehabitacion;
    }
}
