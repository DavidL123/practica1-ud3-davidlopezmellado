/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package com.davidlm.hibernate.base;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

/**
 * Clase Pedidos. Con sus correspondientes campos.
 */
@Entity
@Table(name = "pedidos", schema = "basehotel", catalog = "")
public class Pedido {
    private int id;
    private String platoComida;
    private String bebida;
    private Date fechaConsumicion;
    private double precioTotal;
    private Cliente cliente;

    /**
     * Indica que es el campo id de la tabla, también el nombre de la columna, el método para obtener el dato y el método para modificarlo.
     * @return
     */
    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    /**
     * Indica que es un atributo de la tabla(plato_comida), también el nombre de la columna, el método para obtener el dato y modificarlo.
     *
     * @return
     */
    @Basic
    @Column(name = "plato_comida")
    public String getPlatoComida() {
        return platoComida;
    }

    public void setPlatoComida(String platoComida) {
        this.platoComida = platoComida;
    }

    /**
     * Indica que es un atributo de la tabla(bebida), también el nombre de la columna, el método para obtener el dato y modificarlo.
     *
     * @return
     */
    @Basic
    @Column(name = "bebida")
    public String getBebida() {
        return bebida;
    }

    public void setBebida(String bebida) {
        this.bebida = bebida;
    }

    /**
     * Indica que es un atributo de la tabla(fecha_consumicion), también el nombre de la columna, el método para obtener el dato y modificarlo.
     *
     * @return
     */
    @Basic
    @Column(name = "fecha_consumicion")
    public Date getFechaConsumicion() {
        return fechaConsumicion;
    }

    public void setFechaConsumicion(Date fechaConsumicion) {
        this.fechaConsumicion = fechaConsumicion;
    }

    /**
     * Indica que es un atributo de la tabla(precio_total), también el nombre de la columna, el método para obtener el dato y modificarlo.
     *
     * @return
     */
    @Basic
    @Column(name = "precio_total")
    public double getPrecioTotal() {
        return precioTotal;
    }

    public void setPrecioTotal(double precioTotal) {
        this.precioTotal = precioTotal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pedido pedido = (Pedido) o;
        return id == pedido.id &&
                Double.compare(pedido.precioTotal, precioTotal) == 0 &&
                Objects.equals(platoComida, pedido.platoComida) &&
                Objects.equals(bebida, pedido.bebida) &&
                Objects.equals(fechaConsumicion, pedido.fechaConsumicion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, platoComida, bebida, fechaConsumicion, precioTotal);
    }

    /**
     * Relación de muchos a uno. Un pedido sólo puede tener un cliente.
     * @return
     */
    @ManyToOne
    @JoinColumn(name = "idcliente", referencedColumnName = "id", nullable = false)
    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
}
